class MyThread extends Thread {

    // Override method of run()
    public void run() {
        // Initialize some data into variable of String class: "text"
        String text="Hello KSHRD!\n" +
                    "************************************\n" +
                    "I will try my best to be here at HRD.\n" +
                    "------------------------------------\n" +
                    "Downloading...........Completed 100%!";

        /*
            - Used for loop to iterate through data of String variable "text"\
            - Used String.length() method to get length of "text"
            - Used variable of char datatype to show each character one by one
            - Used Thread to show each character by duration that has set
         */
        for (int i=0; i<text.length(); i++) {
            char letter = text.charAt(i);
            // If loop run till last 16 of character it will set duration to 0ms
            if (i > text.length() - 16) {
                System.out.print(letter);
                try {
                    sleep(0);
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
            }
            // Set duration only 400ms to show each character
            else {
                System.out.print(letter);
                try {
                    sleep(400);
                }catch (InterruptedException ex) {
                    System.out.println(ex);
                }
            }
        }
    }

}

public class Homework005 {
    // Main Method
    public static void main(String[] args) {
        // Instantiated object from MyThread class
        MyThread myThread = new MyThread();
        // Call super class's method to start running Thread
        myThread.start();
    }
}
